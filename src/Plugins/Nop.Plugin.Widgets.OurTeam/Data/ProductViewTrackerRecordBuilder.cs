﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator.Builders.Create.Table;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.Widgets.OurTeam.Domain;
using Nop.Data.Extensions;

namespace Nop.Plugin.Widgets.OurTeam.Data
{
    public class ProductViewTrackerRecordBuilder : NopEntityBuilder<Member>

    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table.WithColumn(nameof(Member.Id)).AsInt32().PrimaryKey()
                //.WithColumn(nameof(Member.MemberId)).AsInt32().ForeignKey<Member>(onDelete: Rule.Cascade)
                .WithColumn(nameof(Member.Name)).AsString(400)
                .WithColumn(nameof(Member.Description)).AsString(400);

        }
    }
}
