﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Widgets.OurTeam.Domain
{
    public class Member : BaseEntity
    {
        public int MemberId { get; set; }
        public int Name { get; set; }
        public string Description { get; set; }

    }
}
