﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.OurTeam.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Widgets.OurTeam.Controllers
{
    [Area(AreaNames.Admin)]
    [AuthorizeAdmin]
    [AutoValidateAntiforgeryToken]
    public class OurTeamController : BasePluginController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public OurTeamController(
            ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        #endregion

        #region Methods
        
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IActionResult> Configure()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var ourTeamSettings = await _settingService.LoadSettingAsync<OurTeamSettings>(storeScope);

            var model = new ConfigurationModel
            {
                TeamId = ourTeamSettings.TeamId,
                TrackingScript = ourTeamSettings.TrackingScript,
                EnableEcommerce = ourTeamSettings.EnableEcommerce,
                UseJsToSendEcommerceInfo = ourTeamSettings.UseJsToSendEcommerceInfo,
                IncludingTax = ourTeamSettings.IncludingTax,
                IncludeCustomerId = ourTeamSettings.IncludeCustomerId,
                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope > 0)
            {
                model.GoogleId_OverrideForStore = await _settingService.SettingExistsAsync(ourTeamSettings, x => x.TeamId, storeScope);
                model.TrackingScript_OverrideForStore = await _settingService.SettingExistsAsync(ourTeamSettings, x => x.TrackingScript, storeScope);
                model.EnableEcommerce_OverrideForStore = await _settingService.SettingExistsAsync(ourTeamSettings, x => x.EnableEcommerce, storeScope);
                model.UseJsToSendEcommerceInfo_OverrideForStore = await _settingService.SettingExistsAsync(ourTeamSettings, x => x.UseJsToSendEcommerceInfo, storeScope);
                model.IncludingTax_OverrideForStore = await _settingService.SettingExistsAsync(ourTeamSettings, x => x.IncludingTax, storeScope);
                model.IncludeCustomerId_OverrideForStore = await _settingService.SettingExistsAsync(ourTeamSettings, x => x.IncludeCustomerId, storeScope);
            }

            return View("~/Plugins/Widgets.OurTeam/Views/Configure.cshtml", model);
        }

        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IActionResult> Configure(ConfigurationModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var ourTeamSettings = await _settingService.LoadSettingAsync<OurTeamSettings>(storeScope);

            //var pictureList = ourTeamSettings.Picture1Id.Split(','); //1 2 3 4 5
            
            ourTeamSettings.TeamId = model.TeamId;
            ourTeamSettings.TrackingScript = model.TrackingScript;
            ourTeamSettings.EnableEcommerce = model.EnableEcommerce;
            ourTeamSettings.UseJsToSendEcommerceInfo = model.UseJsToSendEcommerceInfo;
            ourTeamSettings.IncludingTax = model.IncludingTax;
            ourTeamSettings.IncludeCustomerId = model.IncludeCustomerId;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            await _settingService.SaveSettingOverridablePerStoreAsync(ourTeamSettings, x => x.TeamId, model.GoogleId_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(ourTeamSettings, x => x.TrackingScript, model.TrackingScript_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(ourTeamSettings, x => x.EnableEcommerce, model.EnableEcommerce_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(ourTeamSettings, x => x.UseJsToSendEcommerceInfo, model.UseJsToSendEcommerceInfo_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(ourTeamSettings, x => x.IncludingTax, model.IncludingTax_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(ourTeamSettings, x => x.IncludeCustomerId, model.IncludeCustomerId_OverrideForStore, storeScope, false);

            //now clear settings cache
            await _settingService.ClearCacheAsync();

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

            return await Configure();
        }

        #endregion
    }
}