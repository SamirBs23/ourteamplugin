﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Internal;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Stores;
using Nop.Core.Events;
using Nop.Data;
using Nop.Plugin.Widgets.OurTeam.Domain;

namespace Nop.Plugin.Widgets.OurTeam.Service
{
   public interface IMemberService
    {
        void Log(Member record);
        
    }


    public class MemberService : IMemberService
    {
        private readonly IRepository<Member> _memberRecordRepository;

        public MemberService(IRepository<Member> memberRecordRepository)
        {
            _memberRecordRepository = memberRecordRepository;
        }
        public async void Log(Member record)
        {
            if (record == null)
                throw new ArgumentNullException(nameof(record));
            await _memberRecordRepository.InsertAsync(record);
        }
    }
}
