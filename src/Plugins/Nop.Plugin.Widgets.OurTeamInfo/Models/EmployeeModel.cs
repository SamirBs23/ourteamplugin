﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.OurTeamInfo.Domain
{
   public record EmployeeModel : BaseNopEntityModel
    {
        public string EmpName { get; set; }

      
        public string PictureUrl { get; set; }


        public string Url { get; set; }

        public string ShortDescription { get; set; }

    }
}
