﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.OurTeam
{
    public class OurTeamSettings : ISettings
    {
        public int PictureId { get; set; }
        public string Name { get; set; }

        public string Designation { get; set; }

        //public bool EnableSlider { get; set; }


    }
}