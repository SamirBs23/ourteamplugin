﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Plugins;
using Nop.Services.Security;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Framework.Menu;


namespace Nop.Plugin.Widgets.OurTeam
{
    /// <summary>
    /// Google Analytics plugin
    /// </summary>
    public class OurTeamPlugin : BasePlugin, IWidgetPlugin
    {
        #region Fields



        private readonly IWebHelper _webHelper;
        private readonly INopFileProvider _fileProvider;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public OurTeamPlugin(IWebHelper webHelper, INopFileProvider fileProvider, IPictureService pictureService, ISettingService settingService, ILocalizationService localizationService)
        {
            _webHelper = webHelper;
            _fileProvider = fileProvider;
            _pictureService = pictureService;
            _settingService = settingService;
            _localizationService = localizationService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the widget zones
        /// </returns>
        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { PublicWidgetZones.HeadHtmlTag });
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/OurTeam/Configure";
        }

        /// <summary>
        /// Gets a name of a view component for displaying widget
        /// </summary>
        /// <param name="widgetZone">Name of the widget zone</param>
        /// <returns>View component name</returns>
        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "OurTeam";
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        /// 

        protected async Task CreateSampleDataAsync()
        {
            //pictures
            var sampleImagesPath = _fileProvider.MapPath("~/Plugins/Widgets.OurTeam/Content/images/");

            var profiles = new OurTeamSettings()
            {
                PictureId = (await _pictureService.InsertPictureAsync(await _fileProvider.ReadAllBytesAsync(_fileProvider.Combine(sampleImagesPath, "img-1.jpg")), MimeTypes.ImagePJpeg, "img-1.jpg")).Id,
                Name = "Samir",
                Designation = "Associate Software Engineer",
               
            };
            await _settingService.SaveSettingAsync(profiles);
            profiles = new OurTeamSettings()
            {
                PictureId = (await _pictureService.InsertPictureAsync(await _fileProvider.ReadAllBytesAsync(_fileProvider.Combine(sampleImagesPath, "img-2.jpg")), MimeTypes.ImagePJpeg, "img-2.jpg")).Id,
                Name = "Asif",
                Designation = "Associate Software Engineer",
                
            };
            await _settingService.SaveSettingAsync(profiles);
            profiles = new OurTeamSettings()
            {
                PictureId = (await _pictureService.InsertPictureAsync(await _fileProvider.ReadAllBytesAsync(_fileProvider.Combine(sampleImagesPath, "img-3.jpg")), MimeTypes.ImagePJpeg, "img-3.jpg")).Id,
                Name = "Rafid",
                Designation = "Associate Software Engineer",
               
            };
            await _settingService.SaveSettingAsync(profiles);

            await _localizationService.AddLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Widgets.OurTeam.Picture"] = "Picture",
                ["Plugins.Widgets.OurTeam.Picture.Hint"] = "Upload picture.",
                ["Plugins.Widgets.OurTeam.Name"] = "Comment",
                ["Plugins.Widgets.OurTeam.Name.Hint"] = "Enter comment for picture. Leave empty if you don't want to display any text.",
                ["Plugins.Widgets.OurTeam.ProfileLink"] = "Profile URL",
                ["Plugins.Widgets.OurTeam.ProfileLink.Hint"] = "Enter URL. Leave empty if you don't want this picture to be clickable.",
                ["Plugins.Widgets.OurTeam.Designation"] = "Designation",
                ["Plugins.Widgets.OurTeam.Designation.Hint"] = "Enter Designation."
            });

        }
        public override async Task InstallAsync()
        {  //await CreateSampleDataAsync();

            await base.InstallAsync();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        public override async Task UninstallAsync()
        {
            //await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.OurTeam");

            await base.UninstallAsync();
        }

        #endregion

        /// <summary>
        /// Gets a value indicating whether to hide this plugin on the widget list page in the admin area
        /// </summary>
        public bool HideInWidgetList => false;
    }
}