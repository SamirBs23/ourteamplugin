﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data.Mapping;
using Nop.Plugin.Widgets.OurTeamInfo.Domain;

namespace Nop.Plugin.Widgets.OurTeamInfo.Data
{
    class BaseNameCompatibility : INameCompatibility
    {

        public Dictionary<Type, string> TableNames => new Dictionary<Type, string>
        {
            { typeof(Slider), "NS_Slider" },
            { typeof(EmpIteam), "NS_SliderItem" }
        };

        public Dictionary<(Type, string), string> ColumnName => new Dictionary<(Type, string), string>
        {
        };
    }
}
