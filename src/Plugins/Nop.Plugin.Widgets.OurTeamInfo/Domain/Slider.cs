﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Stores;

namespace Nop.Plugin.Widgets.OurTeamInfo.Domain
{
    class Slider : BaseEntity, IStoreMappingSupported
    {
        private ICollection<EmpIteam> _EmpIteam;

        public string Name { get; set; }

        public int WidgetZoneId { get; set; }

        public bool ShowBackgroundPicture { get; set; }

        public int BackgroundPictureId { get; set; }

        public int DisplayOrder { get; set; }

        public bool Loop { get; set; }

        public bool Nav { get; set; }

        public bool AutoPlay { get; set; }

        public int AutoPlayTimeout { get; set; }

        public bool AutoPlayHoverPause { get; set; }

        public int StartPosition { get; set; }

        public bool LazyLoad { get; set; }

        public int LazyLoadEager { get; set; }

        public bool Video { get; set; }

        public string AnimateOut { get; set; }

        public string AnimateIn { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public bool LimitedToStores { get; set; }

        public virtual ICollection<EmpIteam> SliderItems
        {
            get => _EmpIteam ?? (_EmpIteam = new List<EmpIteam>());
            set => _EmpIteam = value;
        }
    }
}
