﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Localization;

namespace Nop.Plugin.Widgets.OurTeamInfo.Domain
{
   public class EmpIteam : BaseEntity, ILocalizedEntity
    {
        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public int PictureId { get; set; }

        public string Link { get; set; }

        public int SliderId { get; set; }

        public int DisplayOrder { get; set; }

        public virtual EmployeeModel EmployeeModel { get; set; }

    }
}
